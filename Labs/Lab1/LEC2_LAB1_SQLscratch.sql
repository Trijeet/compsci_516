SELECT COUNT(M.movie_id) as number FROM movies as M, ratings as R, users as U \
WHERE M.movie_id=R.movie_id AND R.user_id=U.user_id AND U.zipcode='27708' AND R.rating>=4 AND M."Action"=True;
-- output: 10, need distinct?

SELECT U.user_id, M.movie_title FROM movies as M, ratings as R, users as U \
WHERE M.movie_id=R.movie_id AND R.user_id=U.user_id AND U.age>68 AND R.rating>=3 AND M.release_date LIKE '%1997' ORDER BY U.user_id, M.movie_title;
-- output: 62 rows

SELECT DISTINCT U.user_id as user_id, COUNT(*) FROM movies as M, ratings as R, users as U\
WHERE M.movie_id=R.movie_id AND R.user_id=U.user_id AND U.age<15 AND M."Horror"=True \
GROUP BY U.user_id HAVING COUNT(*)>1 ORDER BY U.user_id;

--------------------------------------------
SELECT DISTINCT U.user_id as user_id
FROM users as U
WHERE U.user_id IN (
SELECT DISTINCT U.user_id, COUNT(*) FROM movies as M, ratings as R, users as U
WHERE M.movie_id=R.movie_id AND R.user_id=U.user_id AND U.age<15 AND M."Horror"=True
GROUP BY U.user_id HAVING COUNT(*)>1 ORDER BY U.user_id;
);
--------------------------------------------

SELECT DISTINCT U.user_id, M.movie_title FROM(SELECT *,COUNT(1) over (PARTITION BY U.user_id) as occurs FROM movies as M, ratings as R, users as U WHERE M.movie_id=R.movie_id AND R.user_id=U.user_id AND U.age<15 AND M."Horror"=True ORDER BY U.user_id) as t WHERE occurs >1;

WITH subquery AS(
SELECT DISTINCT U.user_id as user_id, COUNT(*) FROM movies as M, ratings as R, users as U
WHERE M.movie_id=R.movie_id AND R.user_id=U.user_id AND U.age<15 AND M."Horror"=True
GROUP BY U.user_id HAVING COUNT(*)>1 ORDER BY U.user_id
) SELECT user_id FROM subquery;
SELECT DISTINCT U.user_id, COUNT(*) FROM movies as M, ratings as R, users as U
WHERE M.movie_id=R.movie_id AND R.user_id=U.user_id AND U.age<15 AND M."Horror"=True
GROUP BY U.user_id HAVING COUNT(*)>1 ORDER BY U.user_id;

 -- AND M1.movie_id!=M2.movie_id 

SELECT DISTINCT U.user_id as user_id
 FROM movies as M1, movies as M2, ratings as R, users as U 
 WHERE (SELECT COUNT(*) FROM movies as M1, movies as M2, ratings as R, users as U WHERE M1.movie_id

 M1.movie_id=R.movie_id AND M2.movie_id=R.movie_id AND U.age<15 AND M1."Horror"=True AND M2."Horror"=True AND R.user_id=U.user_id;

SELECT DISTINCT M1.movie_id, M2.movie_id FROM movies as M1 INNER JOIN users as U1 ON M1.user_id=U1.user_id;

SELECT * FROM movies M1 WHERE (SELECT COUNT(*) FROM MOVIES M2 WHERE M1.movie_id=M2.movie_id)>1;

SELECT COUNT(M.movie_id) as number FROM movies as M, ratings as R, users as U WHERE M.movie_id=R.movie_id AND R.user_id=U.user_id AND R.rating>=4 AND M."Action"=True;


SELECT DISTINCT U1.user_id 
FROM movies M1 JOIN ratings R1 ON M1.movie_id=R1.movie_id, movies M2 JOIN ratings R2 ON M2.movie_id=R2.movie_id,
ratings R1 JOIN users U1 ON R1.user_id=U1.user_id, ratings R2 JOIN users U2 ON R2.user_id=U2.user_id 
WHERE U1.age<15 AND M1."Horror"=True AND M2."Horror"=True AND M1.movie_id!=M2.movie_id ORDER BY U1.user_id;

, M1.movie_title, M2.movie_title
M1.movie_title, M2.movie_title
SELECT DISTINCT U.user_id as user_id FROM users as U WHERE U.user_id IN (SELECT DISTINCT U.user_id, COUNT(*) FROM movies as M, ratings as R, users as U WHERE M.movie_id=R.movie_id AND R.user_id=U.user_id AND U.age<15 AND M."Horror"=True GROUP BY U.user_id HAVING COUNT(*)>1 ORDER BY U.user_id);