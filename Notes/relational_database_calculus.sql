-- All expressions of comparison evaluate to unknown; further, queries only return TRUE evaluations
WHERE VARIABLE IS null
WHERE VARIABLE IS NOT null

