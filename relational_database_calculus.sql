-- All expressions of comparison evaluate to unknown; further, queries only return TRUE evaluations
WHERE VARIABLE IS null
WHERE VARIABLE IS NOT null
 -- Selecting from nested queries
 WITH tabl1 AS (SELECT FROM WHERE) tabl2 AS (SELECT FROM WHERE)
 SELECT tabl1.value, tabl2.value FROM tabl1, tabl2 WHERE
 -- Create a new table from existing one
 SELECT X.value, Y.value INTO NewTableName FROM X,Y, WHERE

