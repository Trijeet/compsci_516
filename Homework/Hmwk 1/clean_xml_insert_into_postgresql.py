#!/usr/bin/env python
# coding: utf-8
import psycopg2
import xml.etree.ElementTree as ET
from lxml import etree
import multiprocessing
from Helper_Funcs import multifuncs

## Preprocess and thread
xml = "DataDump/dblp.xml"
treed = etree.DTD(file="DataDump/dblp.dtd") #validation against the xml
timeout, filecount = 1, 0
erf = []
print('preprocessed')
## Traverse through the tree for articles and auths
for marker, elm in etree.iterparse(xml, tag='article', load_dtd=True):
    try:
        multifuncs.write_iterator_f(elm, filecount)
    except:
        erf.append(elm)
    timeout+=1
    if timeout%250==0:
        process = multiprocessing.Process(target=multifuncs.insert_into_postgres_f, args=(f'Script Executables/articles_xml_to_sql{filecount}.sql',))
        process.start()
        process.join()
        process = multiprocessing.Process(target=multifuncs.insert_into_postgres_f, args=(f'Script Executables/authorships_xml_to_sql{filecount}.sql',))
        process.start()
        process.join()
        filecount+=1
    elm.clear()

############################
## Last record clean up
process = multiprocessing.Process(target=multifuncs.insert_into_postgres_f, args=(f'Script Executables/articles_xml_to_sql{cleaner}.sql',))
process.start()
process.join()
process = multiprocessing.Process(target=multifuncs.insert_into_postgres_f, args=(f'Script Executables/authorships_xml_to_sql{cleaner}.sql',))
process.start()
process.join()
print(len(erf), "errors from the articles")


### duplicates from reading in the goodness above
'''duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/cphysics/ArbonaABMMRTB13, Carles Bona-Casas) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/tpc/XuZZ10, Cheng Zhang 0001) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/remotesensing/YangBWYWQC19, Wei Wang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/concurrency/RiedelLS09, S. Chen) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/isci/HaKKFP15, Sang-Wook Kim) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/jsta/FathiBAA17, M. Alizadeh) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/ieicet/ZhaoLLA16, Jian Liu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/jcisd/WangWGLWYMZTGWB16, Ying Wang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/jcphy/BonaBT09, Carles Bona-Casas) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/corr/abs-1809-00083, Shiwei Sun) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/corr/abs-1903-07949, Bo Zhang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/iet-com/KimKPKP07, J. Kim) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/jcal/LinLLC16, Y.-C. Lai) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/jgim/SongZXXWZ11, Cheng Zhang 0001) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/simpra/ZhangZ07, Cheng Zhang 0001) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/complexity/ZouCHZ19, Yun Zou) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/itm/WangWWZC11, Hong Wang) already exists.'''

## Preprocess and thread
# on the above definition, the articles end after 8408.
# the first 20 or so should throw duplicate errors for authors.
xml = "DataDump/dblp.xml"
treed = etree.DTD(file="DataDump/dblp.dtd") #validation against the xml
timeout = 1
filecount = 10000000000
erf = []
print('preprocessed')
## Traverse through the tree for articles and auths
for marker, elm in etree.iterparse(xml, tag='inproceedings', load_dtd=True):
    try:
        multifuncs.write_iterator_f(elm, filecount)
    except:
        erf.append(elm)
    timeout+=1
    if timeout%1000==0:
        process = multiprocessing.Process(target=multifuncs.insert_into_postgres_f, args=(f'Script Executables/inproceedings_xml_to_sql{filecount}.sql',))
        process.start()
        process.join()
        process = multiprocessing.Process(target=multifuncs.insert_into_postgres_f, args=(f'Script Executables/authorships_xml_to_sql{filecount}.sql',))
        process.start()
        process.join()
        filecount+=1
    elm.clear()

############################
## Catch the last X records
process = multiprocessing.Process(target=multifuncs.insert_into_postgres_f, args=(f'Script Executables/inproceedings_xml_to_sql{filecount}.sql',))
process.start()
process.join()
process = multiprocessing.Process(target=multifuncs.insert_into_postgres_f, args=(f'Script Executables/authorships_xml_to_sql{filecount}.sql',))
process.start()
process.join()

print(len(erf), "errors from inproceedings")

'''duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(www/org/mitre/future, Arnon Rosenthal) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/jmlr/LuttinenI12, Jaakko Luttinen) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/procedia/ZeshanM12, Furkh Zeshan) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/procedia/WangD13, Renzhong Wang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/corr/abs-1303-0793, Simon Busard) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(journals/corr/CortesiCF13, Agostino Cortesi) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/cadgraphics/TsaiC09, Hsien-Tsung Chang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/wise/YangYNHLZ16, Liu Yang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/ococosda/ChenZCCLWD15, Yu Chen) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/csndsp/SkodaAVRSS16, Pavel Skoda) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/pacis/LeeLSL16, Hyejung Lee) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icphm/HanYTLY16, Jinsong Yu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icarcv/GaoS0SGL16, Kaizhou Gao) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/dc/DingD04, Hao Ding) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icsai/WangCWG16, Fu-Cheng Wang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/amps/XuLZXY15, Chen Xu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icaci/LiuLLKY18, Xiaolan Liu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/compsac/LiCXPLL18, Jianwei Liao) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/cscwd/TangWTMC12, Xuemei Tang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/cscwd/SunWST18, Yong Sun) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/iciip/HeZHGS16, Jijun He) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/case/ShenWWW13, K. Wang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/cso/ZhuGZF12, Qing Zhu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/iecon/WuGZWZ16, Shaojie Wu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icnc/LiuYHZJY07, Zhongbo Yu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icnc/YuYWYQW08, Lei Yu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icnc/ZhangWZ14, Weiyu Zhang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icnc/WuZWFCXZT16, Di Wu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/gis/ZhangXYLLL17, Yi Liu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/iske/WanJ0J17, Zhen Jin) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/dbta/ZhouTXXZ10, Wei Zhou) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/nems/XianWWKMI17, Dong F. Wang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/peccs/YangRYR13, Soohyun Yang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/uic/ZhaoCLZ10, Zhongtang Zhao) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/uic/LinWPWWLW17, Yaping Wu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/isscc/ShimKBKLKKLHKPK18, Sangho Lee) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/bmei/LiLL17, Yi Li) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/bmei/JinLZWZLZ17, Hong-Zhi Li) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/bmei/LiuHL17, Jun Liu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/3dor/GubinsSVFDZZCXM19, Xiaohua Wan) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/mfi/FuYMY14, Xiaowei Yi) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icoin/PengAGWP18, Zhenlong Peng) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/isocc/GuanZHGS16, Tianchan Guan) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/liss/MaoXTM16, Yi Mao) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/sc/ChenFWHZLWZGZZY18, Wei Zhang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icpca/GaoHZCHZ13, Jianyuan Zhang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/igarss/ZouWWZ17, Chengyi Wang) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/igarss/HeHZWLC18, Long He) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/dtis/CesariPDMBCFIRG17, J. Cesari) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/icycsee/AnQQQ15, Tianwei Qu) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/ram/LinCZLC17, Zhili Lin) already exists.

duplicate key value violates unique constraint "authorships_pkey"
DETAIL:  Key (pubkey, author)=(conf/iconip/LeeYUL97, K. Lee) already exists.'''