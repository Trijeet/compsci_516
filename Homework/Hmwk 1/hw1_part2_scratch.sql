-- q1 answer 
Copy (WITH 
subquery as(
    SELECT  floor(year/10) as decade, COUNT(*) as num_journals
	FROM articles
	GROUP BY decade
), 
subquery2 as(
	SELECT  floor(year/10) as decade, COUNT(*) as num_confs
	FROM inproceedings
	GROUP BY decade
)     
SELECT (subquery2.decade*10) as decade, num_journals, num_confs
FROM subquery2 JOIN subquery ON(subquery.decade=subquery2.decade)) To 'E:/Fall19 Classes/compsci_516/Hmwk 1/Report_Output/parta.csv' With CSV DELIMITER ',' HEADER;


-- q2
-- -1 to disclude the self?

------------------------------------------------------------------
Copy(WITH
	subauthors as(
		SELECT floor(I0.year/10) as decade, I0.area, A0.pubkey as pk, COUNT(*)-1 as c_collabs
		FROM authorships as A0
		   JOIN inproceedings as I0 ON a0.pubkey=I0.pubkey
		WHERE I0.area!='UNKNOWN'
		GROUP BY decade, area, A0.pubkey
	),
	subjournals as(
		SELECT floor(I0.year/10) as decade, A0.pubkey as pk, COUNT(*)-1 as j_collabs
		FROM authorships as A0
		   JOIN articles as I0 ON a0.pubkey=I0.pubkey
		GROUP BY decade, A0.pubkey
	)
SELECT subauthors.decade*10 as decade, area, AVG(c_collabs+j_collabs)
FROM subauthors 
	JOIN authorships A1 ON (subauthors.pk=A1.pubkey)
	LEFT JOIN (subjournals JOIN authorships A2 ON (subjournals.pk=A2.pubkey)) ON (A1.author=A2.author)
WHERE j_collabs>0 AND c_collabs>0
GROUP BY subauthors.decade, area
ORDER BY subauthors.decade, area) To 'E:/Fall19 Classes/compsci_516/Hmwk 1/Report_Output/partb_final.csv' With CSV DELIMITER ',' HEADER;
------------------------------------------------------------------

-- q3
Copy (WITH 
	subauthors as(
		SELECT inproceedings.pubkey, COUNT(*) as collaborators
		FROM inproceedings JOIN authorships ON (inproceedings.pubkey=authorships.pubkey)
		WHERE inproceedings.area!='UNKNOWN'
		GROUP BY inproceedings.pubkey 
	),
	subquery as(
		SELECT DISTINCT ON(pubkey) floor(year/10) as decade, pubkey, area
		FROM inproceedings
		WHERE area!='UNKNOWN'
	)     
SELECT (subquery.decade*10) as decade, area, AVG(collaborators) as avgcollab
FROM subquery JOIN subauthors ON (subauthors.pubkey=subquery.pubkey)
GROUP BY decade, area
ORDER BY decade, area) To 'E:/Fall19 Classes/compsci_516/Hmwk 1/Report_Output/partb.csv' With CSV DELIMITER ',' HEADER;



-- Do the same operation except with the least squares estimator for the mean
Copy (WITH 
	subauthors as(
		SELECT inproceedings.pubkey, COUNT(*) as collaborators
		FROM inproceedings JOIN authorships ON (inproceedings.pubkey=authorships.pubkey)
		WHERE inproceedings.area!='UNKNOWN'
		GROUP BY inproceedings.pubkey 
	),
	subquery as(
		SELECT DISTINCT ON(pubkey) floor(year/10) as decade, pubkey, area
		FROM inproceedings
		WHERE area!='UNKNOWN'
	)     
SELECT (subquery.decade*10) as decade, area, AVG(collaborators) as avgcollab,
regr_slope(avgcollab,(subquery.decade*10)) as reg_slope
FROM subquery JOIN subauthors ON (subauthors.pubkey=subquery.pubkey)
GROUP BY decade, area
ORDER BY decade, area) To 'E:/Fall19 Classes/compsci_516/Hmwk 1/Report_Output/partc.csv' 
With CSV DELIMITER ',' HEADER;


Copy (
WITH 
	subquery as(
		SELECT DISTINCT ON(I0.pubkey) floor(I0.year/10)*10 as decade, I0.pubkey as pubkey, I0.area as area, COUNT(*) as collab
		FROM authorships as A0
		   JOIN inproceedings as I0 ON a0.pubkey=I0.pubkey
		   JOIN inproceedings as I1 ON I1.pubkey=I0.pubkey
		WHERE I0.area!='UNKNOWN'
		GROUP BY I0.pubkey
	)     
SELECT  area, regr_slope(collab,decade),AVG(collab) as avgcollab,
(SUM(decade*collab) - SUM(decade)*SUM(collab))/(SUM(decade*decade)-SUM(decade)*SUM(decade)) as sumd
FROM subquery
WHERE collab>1
GROUP BY area
) To 'E:/Fall19 Classes/compsci_516/Hmwk 1/Report_Output/partc.csv' With CSV DELIMITER ',' HEADER;

-- group_by the decade and then get a general time trend (just on decades), then regress on area.



WITH
	pub_collabers as (
		SELECT A0.pubkey,floor(I0.year/10)*10 as decade, area, COUNT(*) as collabs
		FROM authorships as A0 JOIN inproceedings as I0 ON (I0.pubkey=A0.pubkey)
		WHERE I0.area!='UNKNOWN'
		GROUP BY decade, area, A0.pubkey
	)
SELECT area, (COUNT(*)*SUM(decade*collabs)/COUNT(*) - SUM(decade)*SUM(collabs)/COUNT(*))/(COUNT(*)*SUM(decade*decade)-SUM(decade)*SUM(decade)) as slope
FROM pub_collabers
GROUP BY area


WHERE collabs>0
JOIN articles as A0 ON (PC.pubkey=A0.pubkey)

WITH
	subauthors as(
		SELECT floor(I0.year/10) as decade, I0.area, A0.pubkey as pk, COUNT(*)-1 as c_collabs
		FROM authorships as A0
		   JOIN inproceedings as I0 ON A0.pubkey=I0.pubkey
		WHERE I0.area!='UNKNOWN'
		GROUP BY decade, area, A0.pubkey
	),
	subjournals as(
		SELECT floor(I0.year/10) as decade, A0.pubkey as pk, COUNT(*)-1 as j_collabs
		FROM authorships as A0
		   JOIN articles as I0 ON a0.pubkey=I0.pubkey
		GROUP BY decade, A0.pubkey
	)
SELECT subauthors.decade*10 as decade, area, AVG(c_collabs+j_collabs)
FROM subauthors 
	JOIN authorships A1 ON (subauthors.pk=A1.pubkey)
	LEFT JOIN (subjournals JOIN authorships A2 ON (subjournals.pk=A2.pubkey)) ON (A1.author=A2.author)
WHERE j_collabs>0 AND c_collabs>0
GROUP BY subauthors.decade, area
ORDER BY subauthors.decade, area


(SUM(decade*collab) - SUM(decade)*SUM(collab))/(SUM(decade*decade)-SUM(decade)*SUM(decade))