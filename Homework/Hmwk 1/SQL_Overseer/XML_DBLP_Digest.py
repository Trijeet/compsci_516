#!/usr/bin/env python
# coding: utf-8

# In[35]:


import psycopg2
import sys
import codecs
from io import StringIO
import lxml.etree as et
import csv
import multiprocessing


# In[2]:


xml = "DataDump/dblp.xml"


# In[ ]:


data = []
strong = ''
# dblp.readline()
with open(xml) as dblp:
    garbage = [next(dblp).strip() for line in range(2)]
    counter=0
    while True:
        if counter==17:
            break
        tmp = next(dblp)
        if tmp=='':
            break
        counter+=1
        strong+=tmp.strip()    
strong+="</dblp>"
strong


# In[5]:


data = []
with open(xml) as dblp:
    garbage = [next(dblp).strip() for line in range(2)]
    data += [next(dblp).strip() for line in range(33)]
strink = ''
for x in data:
    strink+=x
print(strink[-10:]=="</article>")
strink+="</dblp>"
strink


# In[70]:


testy = ''
initial = True
with open(xml) as dblp:
    if initial = True:
        garbage = [next(dblp).strip() for line in range(2)]
    for line in range(110):
        testy += next(dblp).strip()
    if testy[-10]!="</article>":
        for i in range(50):
            tmp = next(dblp).strip()
            if("</article>" in tmp):
                testy+="</article>"
                break
            else:
                testy += tmp
                continue
    if initial:
        testy+="</dblp>"
        initial = False
#     offset = dblp.tell()


# In[2]:


def call_sql_subprocess(fname):
    pass
    return


# In[71]:


def write_next_chonk(root):
    for x in root:
        # inevitable disaster prevention
        try:
            a = x.attrib['key']
        except:
            a = 'null'
        try:
            b = x.find('title').text
        except:
            b = 'null'
        try:
            c = x.find('year').text
        except:
            c = 'null'   
        try:
            d = x.find('journal').text
        except:
            d = 'null'  
        try:
            e = x.find('booktitle').text
        except:
            e = 'null'  
            
        if x.tag=='article':
            with open('Script Executables/articles_xml_to_sql.sql', 'a') as f:
                f.write(f"INSERT INTO Articles (pubkey, title, journal, year) VALUES ({a}, {b}, {d}, {c});\n")
                f.close()
        if x.tag=='inproceedings':
            with open('Script Executables/inproceedings_xml_to_sql.sql', 'a') as f:
                f.write(f"INSERT INTO Inproceedings (pubkey, title, journal, year) VALUES ({a}, {b}, {e}, {c});\n")
                f.close()
        with open('Script Executables/authorships_xml_to_sql.sql', 'a') as f:
            for auth in x.findall('author')[:]:
                try:
                    f.write(f"INSERT INTO Authorships (pubkey, author) VALUES ({a}, {auth.text});\n")
                except:
                    print('malformed author entry') 
    return


# In[72]:


root = None


# In[74]:


def parse_next_chonk(stream):
    try:
        before = "<env>" + stream
        after = before + "</env>"
        after = str(after)
        root = et.fromstring(after)
        write_next_chonk(root)
    except:
        print('cannot parse')
    finally:
        return


# In[5]:


def preprocess_next_chonk(dblp, stream):
    people_are_incompetent = ''
    if (stream[-10]!="</article>")|(stream[-16]!="</inproceedings>"):
        #look ahead check
        for i in range(150):
            tmp = next(dblp).strip()
            if ("</article>" in tmp):
                val1, val2 = tmp.split("</article>")[0],tmp.split("</article>")[1]
                stream+=val1
                stream+="</article>"
                people_are_incompetent = val2
                break
            if ("</inproceedings>" in tmp):
                val1, val2 = tmp.split("</inproceedings>")[0],tmp.split("</inproceedings>")[1]
                stream+=val1
                stream+="</inproceedings>"
                people_are_incompetent = val2
                break
            stream += tmp          
    parse_next_chonk(stream)
    return people_are_incompetent


# In[6]:


def run_next_chonk(dblp, stream, num_lines=17):
    for line in range(num_lines):
        stream += next(dblp).strip()
    people_are_incompetent = preprocess_next_chonk(dblp, stream)
    return people_are_incompetent


# In[7]:


def validate_next_chonk(dblp, feed=''):
    stream=feed
    #garbage collect optimization?
    tmp = next(dblp).strip()
    while ("<article" not in tmp):
        if "<inproceedings" in tmp:
            tmp = "<inproceedings" + next(dblp).strip().split("<inproceedings")[1]
            break
        tmp = next(dblp).strip()
    #possible namespace issue (change second tmp to another)
    tmp = "<article" + tmp.split("<article")[1]
    stream+=tmp
    people_are_incompetent = run_next_chonk(dblp,stream)
    return people_are_incompetent


# In[75]:


## head node run through here
# presets for running
xml = "DataDump/dblp.xml"
with open(xml) as dblp:
    #Start the calling cascade
    people_are_incompetent = validate_next_chonk(dblp)
    people_are_incompetent = validate_next_chonk(dblp,feed=people_are_incompetent)
    people_are_incompetent = validate_next_chonk(dblp,feed=people_are_incompetent)
    print(people_are_incompetent)
    # made it to the top ; loop
#     dblp, people_are_incompetent = validate_next_chonk(dblp, feed=people_are_incompetent)
        


# In[48]:


after = '<env><article mdate="2018-01-07" key="tr/meltdown/s18" publtype="informal"><author>Paul Kocher</author><author>Daniel Genkin</author><author>Daniel Gruss</author><author>Werner Haas</author><author>Mike Hamburg</author><author>Moritz Lipp</author><author>Stefan Mangard</author><author>Thomas Prescher 0002</author><author>Michael Schwarz 0001</author><author>Yuval Yarom</author><title>Spectre Attacks: Exploiting Speculative Execution.</title><journal>meltdownattack.com</journal><year>2018</year><ee>https://spectreattack.com/spectre.pdf</ee></article><article mdate="2018-01-07" key="tr/meltdown/m18" publtype="informal"><author>Moritz Lipp</author><author>Michael Schwarz 0001</author><author>Daniel Gruss</author><author>Thomas Prescher 0002</author><author>Werner Haas</author><author>Stefan Mangard</author><author>Paul Kocher</author><author>Daniel Genkin</author><author>Yuval Yarom</author><author>Mike Hamburg</author><title>Meltdown</title><journal>meltdownattack.com</journal><ee>https://meltdownattack.com/meltdown.pdf</ee><year>2018</year></article></env>'
root = et.fromstring(after)


# In[70]:


for arty in root:
        print(arty.find("title").text)
        print(arty.attrib['key'])
#     for attr in arty:
#         print(attr.tag,attr.text)


# In[ ]:





# In[ ]:





# In[146]:


root = ET.fromstring(strink)


# In[179]:


with open('xml_articles.csv', 'w', newline='') as r:
    writer = csv.writer(r)
    writer.writerow(['pubkey', 'journal', 'title', 'year'])
with open('xml_proceedings.csv', 'w', newline='') as r:
    writer = csv.writer(r)
    writer.writerow(['pubkey', 'booktitle', 'title', 'year'])
with open('xml_authors.csv', 'w', newline='') as r:
    writer = csv.writer(r)
    writer.writerow(['pubkey', 'author'])


# In[180]:


def write_chunks(arts,confs,auths):
    if len(arts)>0:
        with open('xml_articles.csv', 'a', newline='') as r:
            writer = csv.writer(r)
            for record in arts:
                writer.writerow(record)
    if len(confs)>0:
        with open('xml_proceedings.csv', 'a', newline='') as r:
            writer = csv.writer(r)
            for record in arts:
                writer.writerow(record)
    if len(auths)>0:
        with open('xml_authors.csv', 'a', newline='') as r:
            writer = csv.writer(r)
            for record in arts:
                writer.writerow(record)


# In[181]:


def sort_chunk(root):
    arts, confs, auths = [],[],[]
    for x in root:
        if x.tag=='article':
            arts.append([x.attrib['key'],x.find("title").text,x.find("journal").text, x.find("year").text])
    write_chunks(arts,confs,auths)


# In[195]:


sort_chunk(root)
#             print(f"INSERT INTO Articles (pubkey, title, journal, year) VALUES ({x.attrib['key']}, {x.find('title').text}, {x.find('journal').text}, {x.find('year').text});")


# In[214]:


def sort_chunk(root):
    for x in root:
        if x.tag=='article':
            with open('Script Executables/articles_xml_to_sql.sql', 'a') as f:
                f.write(f"INSERT INTO Articles (pubkey, title, journal, year) VALUES ({x.attrib['key']}, {x.find('title').text}, {x.find('journal').text}, {x.find('year').text});\n")
                f.close()
        if x.tag=='inproceedings':
            with open('Script Executables/inproceedings_xml_to_sql.sql', 'a') as f:
                f.write(f"INSERT INTO Inproceedings (pubkey, title, journal, year) VALUES ({x.attrib['key']}, {x.find('title').text}, {x.find('booktitle').text}, {x.find('year').text});\n")
                f.close()
        with open('Script Executables/authorships_xml_to_sql.sql', 'a') as f:
            for auth in x.findall('author'):
                f.write(f"INSERT INTO Authorships (pubkey, author) VALUES ({x.attrib['key']}, {auth.text});\n")
            f.close()                


# In[44]:


for x in root:
    for auth in x.findall('author'):
        print(auth.text)
    break


# In[215]:


sort_chunk(root)


# In[ ]:


dblp.tell()


# In[ ]:


dblp.seek(dblp.tell(),0)


# In[ ]:





# In[22]:


'</article><article>'.split("</article>")[0]+"</article>"

