    WITH 
    subquery as(
        SELECT author, COUNT(*) as c_count
        FROM inproceedings as J INNER JOIN authorshipss as A2 ON (J.pubkey=A2.pubkey)
        WHERE year>=2000 AND author IN (
        SELECT author
        FROM 
        inproceedings as I INNER JOIN authorshipss as A ON (I.pubkey = A.pubkey)
        WHERE Area='Database'
        GROUP BY author
        )
        GROUP BY author
    ), 
    subquery2 as(
        SELECT author, COUNT(*)  as j_count
        FROM articles as J INNER JOIN authorshipss as A2 ON (J.pubkey=A2.pubkey)
        WHERE year>=2000 AND author IN (
        SELECT author
        FROM 
        inproceedings as I INNER JOIN authorshipss as A ON (I.pubkey = A.pubkey)
        WHERE Area='Database'
        GROUP BY author
        )
        GROUP BY author
    )     
    SELECT subquery.author as author, j_count+c_count as cnt FROM subquery JOIN subquery2 ON(subquery2.author=subquery.author) ORDER BY cnt DESC, author ASC LIMIT 5;




            WITH 
        subquery as(
            SELECT author, COUNT(*) as c_count
            FROM inproceedings as J INNER JOIN authorships as A2 ON (J.pubkey=A2.pubkey)
            WHERE author IN (
            SELECT author
            FROM 
            inproceedings as I INNER JOIN authorships as A ON (I.pubkey = A.pubkey)
            GROUP BY author
            )
            GROUP BY author
        ), 
        subquery2 as(
            SELECT author, COUNT(*)  as j_count
            FROM articles as J INNER JOIN authorships as A2 ON (J.pubkey=A2.pubkey)
            WHERE author IN (
            SELECT author
            FROM 
            inproceedings as I INNER JOIN authorships as A ON (I.pubkey = A.pubkey)
            GROUP BY author
            )
            GROUP BY author
        )     
                SELECT COUNT(DISTINCT subquery2.author) as cnt,         
        CASE 
        WHEN c_count != null THEN c_count
        ELSE (0)
        END AS c_count 
        FROM subquery2 LEFT JOIN subquery ON(subquery2.author=subquery.author) WHERE j_count>c_count;



        WITH 
        subquery as(
            SELECT author, COUNT(*) as c_count
            FROM inproceedings as J INNER JOIN authorships as A2 ON (J.pubkey=A2.pubkey)
            WHERE author IN (
            SELECT author
            FROM 
            inproceedings as I INNER JOIN authorships as A ON (I.pubkey = A.pubkey)
            GROUP BY author
            )
            GROUP BY author
        ), 
        subquery2 as(
            SELECT author, COUNT(*)  as j_count
            FROM articles as J INNER JOIN authorships as A2 ON (J.pubkey=A2.pubkey)
            WHERE author IN (
            SELECT author
            FROM 
            inproceedings as I INNER JOIN authorships as A ON (I.pubkey = A.pubkey)
            GROUP BY author
            )
            GROUP BY author
        )     
        SELECT *
        FROM subquery2 LEFT JOIN subquery ON(subquery2.author=subquery.author) WHERE j_count>c_count OR c_count IS NULL LIMIT 5;     

