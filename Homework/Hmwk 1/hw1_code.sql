BEGIN;

-- Conference papers
DROP TABlE IF EXISTS Inproceedings;
CREATE TABLE Inproceedings(
	pubkey TEXT,
	title TEXT,
	booktitle TEXT,
	year INT,
	PRIMARY KEY (pubkey)
);

-- Journal pubs 
DROP TABlE IF EXISTS Articles;
CREATE TABLE Articles(
	pubkey TEXT,
	title TEXT,
	journal TEXT,
	year INT,
	PRIMARY KEY (pubkey)
);

-- Researcher
DROP TABlE IF EXISTS Authorships;
CREATE TABLE Authorships(
	pubkey TEXT,
	author TEXT,
	PRIMARY KEY (pubkey, author)
);

COMMIT;