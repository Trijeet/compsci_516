import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
// Do NOT use different Spark libraries.

object NoInOutLink {
    def main(args: Array[String]) {
        val MASTER_ADDRESS = "ec2-35-153-39-193.compute-1.amazonaws.com"
        val SPARK_MASTER = "spark://" + MASTER_ADDRESS + ":7077"
        val HDFS_MASTER = "hdfs://" + MASTER_ADDRESS + ":9000"
        val input_dir = HDFS_MASTER + "/pagerank/input"
        val links_file = input_dir + "/links-simple-sorted.txt"
        val titles_file = input_dir + "/titles-sorted.txt"
        val OUTPUT_DIR = HDFS_MASTER + "/pagerank/output"
        val num_partitions = 10
        

        val conf = new SparkConf()
            .setAppName("NoInOutLink")
            .setMaster(SPARK_MASTER)
            .set("spark.driver.memory", "1g")
            .set("spark.executor.memory", "2g")

        val sc = new SparkContext(conf)

        val links = sc
            .textFile(links_file, num_partitions)
            .map { line => (line.split(":")(0),line.split(":")(1).split(" ").filter(_.nonEmpty)) }
            .flatMap { pgarr => pgarr._2.map((pgarr._1, _)) }
            .map { tup => (tup._1.toInt, tup._2.toInt) }

        val titles = sc
            .textFile(titles_file, num_partitions)
            .zipWithIndex()
            .map{tup => (tup._2.toInt + 1, tup._1) }

        /* No Outlinks */
        println("[ NO OUTLINKS ]")
        titles.subtractByKey(links.reduceByKey((out1, out2) => out2)
            .sortBy(_._1, ascending=true))
            .take(10)
            .foreach(println)

        /* No Inlinks */
        val no_inlinks = links.map(_.swap)
        println("\n[ NO INLINKS ]")
        titles.subtractByKey(no_inlinks.reduceByKey((out1, out2) => out2))
            .sortBy(_._1, ascending=true)
            .take(10)
            .foreach(println)
    }
}
