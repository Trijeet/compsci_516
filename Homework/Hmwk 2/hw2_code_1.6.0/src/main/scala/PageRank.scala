import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
// Do NOT use different Spark libraries.

object PageRank {
    def main(args: Array[String]) {
        val MASTER_ADDRESS = "ec2-35-153-39-193.compute-1.amazonaws.com"
        val SPARK_MASTER = "spark://" + MASTER_ADDRESS + ":7077"
        val HDFS_MASTER = "hdfs://" + MASTER_ADDRESS + ":9000"
        val input_dir = HDFS_MASTER + "/pagerank/input"
        val links_file = input_dir + "/links-simple-sorted.txt"
        val titles_file = input_dir + "/titles-sorted.txt"
        val OUTPUT_DIR = HDFS_MASTER + "/pagerank/output"
        val num_partitions = 10
        val iters = 10

        val conf = new SparkConf()
            .setAppName("PageRank")
            .setMaster(SPARK_MASTER)
            .set("spark.driver.memory", "1g")
            .set("spark.executor.memory", "2g")

        val sc = new SparkContext(conf)

        val links = sc
            .textFile(links_file, num_partitions)
            .map { line => (line.split(":")(0),line.split(":")(1).split(" ").filter(_.nonEmpty)) }
            .flatMap { pgarr => pgarr._2.map((pgarr._1, _)) }
            .map { tup => (tup._1.toInt, tup._2.toInt) }

        val titles = sc
            .textFile(titles_file, num_partitions)
            .zipWithIndex()
            .map{tup => (tup._2.toInt + 1, tup._1) }

        /* PageRank */

        //Set the constants:
        //iters moved to head of file
        val d = 0.85 //set to 0.82 for the same performance.
        val N = titles.count
        val linkz = links.distinct().groupByKey().cache() //matche for outlinks
        // var outy = links.countByKey() //mapper
        // outy += 1 -> 0

        //Initialize the ranks as given by formula:
        var pr_next: Double = 100.0/N;
        var current_ranks = titles
            .map(tup => (tup._1, (tup._2, pr_next)));
        
        //Run for convergence:
        for (i <- 1 to iters) {
            var rankz = current_ranks
                .map{ case (pg, (title,crank)) => (pg, crank)}
            val inlinks = linkz.join(rankz)
                .values                                    
                .flatMap{ case (edges, rank) =>  
                    val nlinks = edges.size        
                    edges.map(url => (url, rank / nlinks))
            }
            current_ranks = current_ranks
                .leftOuterJoin(inlinks.reduceByKey(_ + _))
                .map { case (page, ((title, rank), update)) => 
                        val bupdate = update.getOrElse(0).asInstanceOf[Number].doubleValue
                        (page, (title, (1-d)/N*100+d*bupdate))
            }
        }
        //Normalization step for probability - omitted just for ordering.
        //Print out the top 10 weights, descending:
        println("[ PageRanks ]")
        current_ranks
            .sortBy(_._2._2, ascending=false)
            .take(10)
            .foreach(println)
    }
}
